//
//  NewTaskViewController.swift
//  Module14
//
//  Created by Max Yalounin on 28.03.2021.
//

import UIKit
import RealmSwift

// vc, который появляется при нажатии на кнопку "+"
class NewTaskRealmViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var taskTextField: UITextField!
    @IBOutlet weak var addButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        addButton.isEnabled = false
        // объявляем vc делегатом текстового поля
        taskTextField.delegate = self
        
        // отображаем клавиатуру и курсор в текстовом поле при открытии VC
        taskTextField.becomeFirstResponder()
    }

    
    // кнопка "Добавить"
    @IBAction func addTask(_ sender: Any) {
        
        guard let text = taskTextField.text  else {return}
        
        // создаем новый task и заносим его в БД
        let task = RealmTaskModel()
        task.taskText = text
        task.date = Date()

        RealmLoader.shared.addTask(task: task)
        
        // закрываем VC
        navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    // во избежание добавления пустых записей делаем кнопку "Добавить" неактивной при пустом текстовом поле
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if let text = textField.text, !text.isEmpty {
            addButton.isEnabled = true
        } else {
            addButton.isEnabled = false
        }
        
    }
    
}
