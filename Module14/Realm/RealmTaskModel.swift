//
//  Task.swift
//  Module14
//
//  Created by Max Yalounin on 28.03.2021.
//

import Foundation
import RealmSwift
// модель для БД
class RealmTaskModel: Object {
    @objc dynamic var taskText = ""
    @objc dynamic var date = Date() // дата
    @objc dynamic var dateString: String{ // дата в строковом формате (вычисляемая из Date)
        get {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy hh:mm"
            
            return formatter.string(from: self.date)
        }
    }
}
