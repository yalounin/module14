//
//  TodoTableViewCell.swift
//  Module14
//
//  Created by Max Yalounin on 20.03.2021.
//

import UIKit
// класс строки таблицы
class TodoTableViewCell: UITableViewCell {

    @IBOutlet weak var taskLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
