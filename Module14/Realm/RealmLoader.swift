//
//  RealmLoader.swift
//  Module14
//
//  Created by Max Yalounin on 28.03.2021.
//

import Foundation
import RealmSwift

// класс для работы с БД
class RealmLoader {
    
    static let shared = RealmLoader()
    private let realm = try! Realm()
    
    // добавление новой записи
    func addTask(task: RealmTaskModel) {

        try! realm.write {
            realm.add(task)
        }
        
    }
    
    // выгрузка массива с данными
    func loadTasks() -> [RealmTaskModel] {
        var tasks = [RealmTaskModel]()
        
        for task in realm.objects(RealmTaskModel.self) {
            tasks.append(task)
        }
        
        return tasks
        
    }
    
    // удаление определенной записи из массива
    func deleteTask(task: RealmTaskModel) {
        try! realm.write {
            realm.delete(task)
        }
    }
}
