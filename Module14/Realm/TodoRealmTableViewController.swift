//
//  TodoTableViewController.swift
//  Module14
//
//  Created by Max Yalounin on 20.03.2021.
//

import UIKit
import RealmSwift

// класс с данными в виде таблицы
class TodoRealmTableViewController: UITableViewController {
        
    // массив с данными
    var tasks: [RealmTaskModel] = []
    
    
    // при появлении вью прогрузить массив из БД
    override func viewWillAppear(_ animated: Bool) {
        tasks = RealmLoader.shared.loadTasks()
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    // наполняем строки данными
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TodoTableViewCell
                
        cell.taskLabel!.text = tasks[indexPath.row].taskText
        cell.dateLabel!.text = tasks[indexPath.row].dateString

        return cell
    }
    
    // удаление по свайпу влево
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let task = tasks[indexPath.row]
            // удаляем из БД соответствующую запись
            RealmLoader.shared.deleteTask(task: task)
            // обновляем массив из БД
            tasks = RealmLoader.shared.loadTasks()

            // строка исчезает
            tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.reloadData()


        }
    }
    
}


