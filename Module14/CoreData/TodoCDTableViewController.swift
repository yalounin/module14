//
//  TodoCDTableViewController.swift
//  Module14
//
//  Created by Max Yalounin on 28.03.2021.
//

import UIKit
import CoreData

class TodoCDTableViewController: UITableViewController {

    // массив для заполнения таблицы
    var tasks: [Task] = []
    
    // кнопка + на панели навигации
    @IBAction func addTask(_ sender: UIBarButtonItem) {
        
        // добавляем новую запись через AlertController
        let alertController = UIAlertController(title: "Новая задача", message: "Введите текст задачи", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Сохранить", style: .default, handler: {action in
            // инициализируем текстовое поле в алерт
            let textField = alertController.textFields?.first
            
            if let newTask = textField?.text {
                // сохраняем данные в CoreData
                self.saveTask(title: newTask)
                self.tableView.reloadData()
            }
            
        })
        // добавляем textField
        alertController.addTextField(configurationHandler: {_ in})
        
        // инициализируем кнопку отмены
        let cancelAction = UIAlertAction(title: "Отмена", style: .default, handler: {_ in})
        
        // добавляем кнопки в алерт
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        
        // показать алерт
        present(alertController, animated: true, completion: nil)
        
    }
    
    // сохранить данные в БД
    func saveTask(title: String) {
        // достаем контекст CoreData через делегата
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        // определяем представление объектов в БД
        guard let entity = NSEntityDescription.entity(forEntityName: "Task", in: context) else {return}
        
        // объект БД
        let taskObject = Task(entity: entity, insertInto: context)
        
        // присваиваем переданное значение (название) объекту
        taskObject.title = title as String
        
        // сохраняем контекст и добавляем созданный объект в массив tasks
        do {
            try context.save()
            tasks.append(taskObject)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    // загрузка данных из БД
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
                
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        // запрашиваемые данные
        let fetchRequest: NSFetchRequest<Task> = Task.fetchRequest()
        
        // наполняем массив данными из контекста
        do {
            tasks = try context.fetch(fetchRequest)
            
        } catch let error as NSError {
            print(error.localizedDescription)

        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }

    // инициализируем строки из данных массива
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        let task = tasks[indexPath.row]
        
        cell.textLabel?.text = task.title                
        
        return cell
    }
    
    // удаление строки по свайпу
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let task = tasks[indexPath.row]

            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            
            // удаление из контекста (из БД)
            context.delete(task)
            // удаление из массива
            tasks.remove(at: indexPath.row)
            
            // сохранение БД
            do {
                try context.save()
            } catch let error as NSError {
                print(error.localizedDescription)
            }
            
            // удаление строки tableView
            tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.reloadData()


        }
    }
    


}
