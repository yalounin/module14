//
//  NameViewController.swift
//  Module14
//
//  Created by Max Yalounin on 16.03.2021.
//

import UIKit

class NameViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // объявляем vc делегатом для текстовых полей
        nameTextField.delegate = self
        surnameTextField.delegate = self

    }
    
    // при появлении вью выгрузить данные из UserDefaults
    override func viewWillAppear(_ animated: Bool) {
        
        nameTextField.text = Persistance.shared.userName
        surnameTextField.text = Persistance.shared.userSurname
        
    }
    
    // при изменении текста в текстовых полях обновить соответствующие данные в UserDefaults
    func textFieldDidChangeSelection(_ textField: UITextField) {
        
            Persistance.shared.userName = self.nameTextField.text
            Persistance.shared.userSurname = self.surnameTextField.text
    }

}
