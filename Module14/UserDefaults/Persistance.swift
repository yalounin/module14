//
//  Persistance.swift
//  Module14
//
//  Created by Max Yalounin on 16.03.2021.
//

import Foundation

class Persistance {
    static let shared = Persistance()
    
    // ключи, по которым идентифицируются данные
    private let userNameKey = "userNameKey"
    private let userSurnameKey = "userSurnameKey"
    
    // свойство, содержащее фамилию из текстового поля
    var userName: String? {
        set { UserDefaults.standard.setValue(newValue, forKey: userNameKey )
        }
        get { UserDefaults.standard.string(forKey: userNameKey)
        }
    }
    
    // свойство, содержащее фамилию
    var userSurname: String? {
        set { UserDefaults.standard.setValue(newValue, forKey: userSurnameKey )

        }
        get { UserDefaults.standard.string(forKey: userSurnameKey) }
    }
    
        
        
    
}
