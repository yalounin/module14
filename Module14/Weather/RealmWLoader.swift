//
//  RealmWLoader.swift
//  Module14
//
//  Created by Max Yalounin on 29.03.2021.
//

import Foundation
import RealmSwift

// класс для работы с БД
class RealmWLoader {
    static let shared = RealmWLoader()
    
    let realm = try! Realm()
   
    // обновить данные в БД
    func updateWeather(weather: Weather) {
            try! self.realm.write {
                self.realm.deleteAll()
                self.realm.add(weather) // данные состоят из 1 элемента
            }
    }
    
    // подгружаем данные из БД до загрузки json
    func loadWeather() -> Weather {
            let w = Weather()
            
            return realm.objects(Weather.self).first ?? w // если ничего нет, то пустая строка
           
    }
}
