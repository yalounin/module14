import Foundation
import RealmSwift

// модель объекта БД
class Weather: Object {
    @objc dynamic var temperature: String // температура воздуха в строковом формате
    
    // инициализатор на случай отсутствия данных в БД
    override init() {
        self.temperature = ""
    }

}
