//
//  WeatherViewController.swift
//  Module14
//
//  Created by Max Yalounin on 29.03.2021.
//

import UIKit

// VC для отображения погоды
class WeatherViewController: UIViewController {
    
    @IBOutlet weak var currentWeatherLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // перед загрузкой json, отображаем данные из БД
        let w = RealmWLoader.shared.loadWeather()
        currentWeatherLabel.text = w.temperature
        // создаем лоадер, объявляем VC его делегатом, грузим данные из json
        let loader = WeatherLoader()
        loader.delegate = self
        loader.codableLoader()
        }
}


// подписываем протокол
extension WeatherViewController: LoaderDelegate {
    // подгружаем данные о текущей погоде
    func loaded(temperature: String) {
        DispatchQueue.main.async {
        // передаем данные из делегата
        self.currentWeatherLabel.text = temperature
        let w = Weather()
        w.temperature = temperature
        // сохраняем загруженные из json данные в БД
        RealmWLoader.shared.updateWeather(weather: w)
        }
    }

}
