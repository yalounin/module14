//
//  WeatherModel.swift
//  Module14
//
//  Created by Max Yalounin on 29.03.2021.
//

import Foundation

// данные для парсинга (json содержит много полей, но в структурах - только нужные для задачи)
struct WeatherModel: Codable {
    var current: CurrentWeather?
}

struct CurrentWeather: Codable {
    var temp: Double?
    
}
