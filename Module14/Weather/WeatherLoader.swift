

import Foundation
import Alamofire

// протокол для передачи текущей температуры в WeatherViewController
protocol LoaderDelegate {
    func loaded(temperature: String)
}


// класс-загрузчик данных
class WeatherLoader {
    var url = "https://api.openweathermap.org/data/2.5/onecall?lat=55.45&lon=37.36&exclude=minutely,hourly,alerts&appid="
    var apiKey = "b26bb6eefba746908f9de1ec6dd4a200"
    
    var delegate: LoaderDelegate?
    
    // декодирование json
    func codableLoader() {
        // инициализация url-запроса
        let url = URL(string: self.url + apiKey)!
        let request = URLRequest(url: url)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {return}
            
            var result: WeatherModel?
            
            // декодируем модель
            do {
                result = try JSONDecoder().decode(WeatherModel.self, from: data)
            } catch {
                print(error.localizedDescription)
            }
            
            guard let final = result, let temp = final.current?.temp else {return}
            
            // переводим из кельвинов в градусы
            let res = Int(temp) - 273
            
            // передаем делегату данные
            self.delegate?.loaded(temperature: "\(res)°C")
    }
        task.resume()

}


}
